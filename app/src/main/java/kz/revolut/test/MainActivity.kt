package kz.revolut.test

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kz.revolut.test.converter.ui.ConverterFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        if (savedInstanceState == null) {
            setFragment()
        }
    }

    fun setFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, ConverterFragment())
            .commitNow()
    }
}