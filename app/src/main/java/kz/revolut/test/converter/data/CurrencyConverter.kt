package kz.revolut.test.converter.data

import kz.revolut.test.converter.data.model.Currency
import kz.revolut.test.core.data.Convertable

interface CurrencyConverter {

    var enteredAmount: Float?
    var selectedCurrency: Currency?

    fun convert(
        list: List<Convertable>,
        amount: Float? = null,
        originCurrency: Currency? = null
    ): List<Convertable>
}

class CurrencyConverterImpl: CurrencyConverter {

    companion object {
        private const val DEFAULT_AMOUNT = 1.0f
    }

    override var enteredAmount: Float? = null
    override var selectedCurrency: Currency? = null

    override fun convert(list: List<Convertable>, amount: Float?, originCurrency: Currency?): List<Convertable> {
        amount?.let {
            enteredAmount = it
        }

        originCurrency?.let {
            selectedCurrency = it
        }

        list.forEach {
            it.convert(selectedCurrency, enteredAmount ?: DEFAULT_AMOUNT)
        }

        return list
    }
}