package kz.revolut.test.converter.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kz.revolut.test.core.data.Convertable

@Parcelize
data class Currency (
    val code: String,
    var rate: Float,
    val isBase: Boolean,
    var convertedAmount: Float = rate
) : Parcelable, Convertable {

    override fun convert(originCurrency: Currency?, amount: Float) {
        convertedAmount = if (originCurrency == null) {
            amount * rate
        } else {
            if (originCurrency.isBase && isBase) {
                amount * rate
            } else {
                if (isBase) {
                    (amount / originCurrency.rate)
                } else {
                    (amount / originCurrency.rate) * rate
                }
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other is Currency) {
            return this.code == other.code && this.rate == other.rate && this.isBase == other.isBase
        }
        return false
    }
}