package kz.revolut.test.converter.data.network

import kz.revolut.test.converter.data.model.Currency
import kz.revolut.test.core.data.api.ApiCurrency

object CurrencyAPIMapper {

    fun map(apiCurrency: ApiCurrency): List<Currency> {
        val currencyList = arrayListOf<Currency>()
        currencyList.add(Currency(apiCurrency.base, 1.0f, true))

        apiCurrency.rates.map {
            val currency = map(it)
            currencyList.add(currency)
        }

        return currencyList
    }

    private fun map(currencyMap: Map.Entry<String, Float>) =
        Currency(currencyMap.key, currencyMap.value, false)
}