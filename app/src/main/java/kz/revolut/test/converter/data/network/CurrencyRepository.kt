package kz.revolut.test.converter.data.network

import kz.revolut.test.converter.data.model.Currency
import kz.revolut.test.core.data.BaseRepository
import kz.revolut.test.core.data.Result

interface CurrencyRepository {
    suspend fun getLatestRates(base: String): Result<List<Currency>>
}

open class CurrencyAPIRepository(
    private val currencyService: CurrencyService
): BaseRepository(), CurrencyRepository {

    override suspend fun getLatestRates(base: String): Result<List<Currency>> {
        return safeApiCall({
            CurrencyAPIMapper.map(currencyService.getLatestCurrencyRates(base))
        })
    }
}