package kz.revolut.test.converter.data.network

import kz.revolut.test.core.data.api.ApiCurrency
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyService {

    @GET("latest")
    suspend fun getLatestCurrencyRates(@Query("base") base: String): ApiCurrency

}