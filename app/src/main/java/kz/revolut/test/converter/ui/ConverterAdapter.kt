package kz.revolut.test.converter.ui

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kz.revolut.test.R
import kz.revolut.test.converter.data.model.Currency
import kz.revolut.test.databinding.ItemConverterBinding

class ConverterAdapter(
    val imm: InputMethodManager,
    val onItemSelected: ((Int) -> Unit),
    val onAmountChanged: ((Currency, Float) -> Unit)
): ListAdapter<Currency, ConverterAdapter.CurrencyViewHolder>(CurrencyItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CurrencyViewHolder (
        DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_converter, parent, false)
    )

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class CurrencyViewHolder(
        private val binding: ItemConverterBinding
    ): RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                binding.amountField.requestFocus()
            }

            binding.amountField.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
                binding.amountField.setSelection(binding.amountField.text.count())

                if (!hasFocus) {
                    return@OnFocusChangeListener
                }

                if (hasFocus) {
                    onItemSelected(adapterPosition)
                    imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT)
                }
            }

            binding.amountField.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {

                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    try {
                        if (binding.amountField.isFocused) {
                            val stringAmount = if (s.isNullOrEmpty()) "0" else s.toString()
                            val amount = stringAmount.toFloat()
                            val item = getItem(adapterPosition)
                            onAmountChanged(item, amount)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
        }

        fun bind(item: Currency) {
            binding.currency = item
            binding.currencyFlag.setImageResource(getFlagResId(item.code))

            if (!binding.amountField.isFocused) {
                binding.amount = item.convertedAmount
            }
        }

        private fun getFlagResId(currencyCode: String): Int {
            val resources = binding.root.context.resources
            val flagName = "ic_" + currencyCode.toLowerCase() + "_flag"
            return resources.getIdentifier(flagName, "drawable", binding.root.context.packageName)
        }
    }
}

class CurrencyItemDiffCallback: DiffUtil.ItemCallback<Currency>() {

    override fun areItemsTheSame(oldItem: Currency, newItem: Currency): Boolean {
        return oldItem.code == newItem.code
    }

    override fun areContentsTheSame(oldItem: Currency, newItem: Currency): Boolean {
        return oldItem.convertedAmount == newItem.convertedAmount
    }
}