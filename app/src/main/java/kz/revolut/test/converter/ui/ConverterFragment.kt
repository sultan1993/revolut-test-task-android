package kz.revolut.test.converter.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import com.google.android.material.snackbar.Snackbar
import kz.revolut.test.R
import kz.revolut.test.converter.data.CurrencyConverterImpl
import kz.revolut.test.converter.data.network.CurrencyAPIRepository
import kz.revolut.test.core.utils.ApiClient
import kz.revolut.test.databinding.FragmentConverterBinding

class ConverterFragment : Fragment() {

    private lateinit var binding: FragmentConverterBinding
    private lateinit var converterViewModel: ConverterViewModel

    private val converterAdapter: ConverterAdapter by lazy {
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        ConverterAdapter(imm, converterViewModel::selectItem, converterViewModel::onAmountChanged)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val currencyConverter = CurrencyConverterImpl()
        val repository = CurrencyAPIRepository(ApiClient.currency)
        val factory = ConverterViewModel.Factory(repository, currencyConverter)
        converterViewModel = ViewModelProviders.of(this, factory)[ConverterViewModel::class.java]
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_converter, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.title = "Revolut Rates"

        with(binding) {
            viewModel = converterViewModel
            currencyList.adapter = converterAdapter
            currencyList.itemAnimator = DefaultItemAnimator().apply {
                supportsChangeAnimations = false
            }
        }

        converterViewModel.converterUI.observe(viewLifecycleOwner, Observer {
            when(it) {
                is ConverterUI.ListLoaded -> {
                    converterAdapter.submitList(it.list)
                }

                is ConverterUI.Error -> {
                    Snackbar
                        .make(binding.root, R.string.error, Snackbar.LENGTH_SHORT)
                        .setAction(R.string.retry) { converterViewModel::startFetchingRates }
                        .show()
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        converterViewModel.startFetchingRates()
    }

    override fun onPause() {
        super.onPause()
        converterViewModel.stopFetchingRates()
    }
}