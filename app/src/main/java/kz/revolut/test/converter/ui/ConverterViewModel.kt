package kz.revolut.test.converter.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kz.revolut.test.R
import kz.revolut.test.converter.data.CurrencyConverter
import kz.revolut.test.converter.data.model.Currency
import kz.revolut.test.converter.data.network.CurrencyRepository
import kz.revolut.test.core.data.Result
import kz.revolut.test.core.lifecycle.SuspendableViewModel
import java.util.*
import kotlin.collections.ArrayList

class ConverterViewModel(
    private val currencyRepository: CurrencyRepository,
    private val currencyConverter: CurrencyConverter
) : SuspendableViewModel() {

    companion object {
        private const val DEFAULT_BASE = "EUR"
        private const val DEFAULT_DELAY = 1000L
    }

    private val _converterUI = MutableLiveData<ConverterUI>()
    internal val converterUI: LiveData<ConverterUI>
        get() = _converterUI

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    private var timer: Timer? = null
    private var currencyList: ArrayList<Currency> = arrayListOf()

    init {
        _loading.value = true
        startFetchingRates()
    }

    fun startFetchingRates() {
        if (timer != null) {
            return
        }

        timer = Timer().apply {
            scheduleAtFixedRate(object : TimerTask() {
                override fun run() {
                    fetchRates()
                }
            }, 0, DEFAULT_DELAY)
        }
    }

    fun stopFetchingRates() {
        timer?.cancel()
        timer = null
        cancelPendingJobs()
    }

    private fun fetchRates() {
        launch(io) {
            when(val result = currencyRepository.getLatestRates(DEFAULT_BASE)) {
                is Result.Success -> {
                    _loading.postValue(false)
                    val refreshedList = refreshList(result.data)
                    val convertedList = currencyConverter.convert(refreshedList) as List<Currency>
                    displayList(convertedList)
                }

                is Result.Error -> {
                    _loading.postValue(false)

                    //we exclude exception that is thrown when coroutine is cancelled
                    if (result.exception !is CancellationException) {
                        _converterUI.postValue(ConverterUI.Error(R.string.error))
                    }
                }
            }
        }
    }

    private fun refreshList(fetchedList: List<Currency>): List<Currency> {
        val refreshedList = ArrayList<Currency>()

        if (currencyList.isEmpty()) {
            refreshedList.addAll(fetchedList)
        } else {
            //update existing list's rates
            currencyList.forEach { current ->
                fetchedList.forEach { new ->
                    if (current.code == new.code) {
                        current.rate = new.rate
                    }
                }
            }

            //if there are difference in lists then add these items to the end of the list
            fetchedList.forEach {
                if (!currencyList.contains(it)) {
                    currencyList.add(it)
                }
            }


            currencyList.forEach {
                refreshedList.add(it.copy())
            }
        }

        return refreshedList
    }

    fun onAmountChanged(item: Currency, amount: Float) {
        val listToConvert = ArrayList(currencyList.map { it.copy() })
        val convertedList = currencyConverter.convert(listToConvert, amount, item) as List<Currency>
        displayList(convertedList)
    }

    fun selectItem(position: Int) {
        //we stop fetching so we won't get multiple operations on UI thread
        stopFetchingRates()

        launch(io) {
            val selectedList = ArrayList(currencyList).apply {
                removeAt(position).also {
                    add(0, it)
                }
            }

            displayList(selectedList)
            delay(1000) // small delay for smooth ui (waiting for an item to appear on top)
            startFetchingRates()
        }
    }

    private fun displayList(list: List<Currency>) {
        launch(main) {
            _converterUI.value = ConverterUI.ListLoaded(list)
            currencyList = ArrayList(list)
        }
    }

    class Factory(
        private val currencyRepository: CurrencyRepository,
        private val currencyConverter: CurrencyConverter
    ): ViewModelProvider.NewInstanceFactory() {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ConverterViewModel(currencyRepository, currencyConverter) as T
        }
    }
}

internal sealed class ConverterUI {
    data class ListLoaded(val list: List<Currency>): ConverterUI()
    data class Error(val messageResId: Int): ConverterUI()
}