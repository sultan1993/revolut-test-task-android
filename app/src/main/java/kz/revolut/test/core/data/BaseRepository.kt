package kz.revolut.test.core.data

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.CancellationException
import retrofit2.HttpException
import java.io.IOException
import kotlin.reflect.KClass

open class BaseRepository {

    suspend fun <T : Any> safeApiCall(call: suspend () -> T,
                                      exceptionClass: KClass<out Exception>? = null,
                                      defaultErrorMessage: String? = null): Result<T> {
        return try {
            Result.Success(call())
        }catch (e: CancellationException) {
            Result.Error(e)
        } catch (e: HttpException) {
            handleHttpException(e, exceptionClass, defaultErrorMessage)
        }  catch (e: Exception) {
            ioFailure(e, defaultErrorMessage)
        }
    }

    private fun handleHttpException(e: HttpException,
                                    kClass: KClass<out Exception>?,
                                    errorMessage: String?): Result.Error {
        val source = e.response()?.errorBody() ?: return ioFailure(e, errorMessage)

        return try {
            val apiException = parseException(source.string(), kClass ?: HttpException::class)
            Result.Error(apiException)
        } catch (ioEx: IOException) {
            ioFailure(ioEx, errorMessage)
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    private fun ioFailure(e: Exception, errorMessage: String?): Result.Error {
        val message = errorMessage ?: e.localizedMessage
        return Result.Error(IOException(message, e))
    }

    private fun parseException(json: String, kClass: KClass<out Exception>): Exception {
        return try {
            Gson().fromJson(json, TypeToken.getParameterized(kClass.java).type)
        } catch (e: Exception) {
            e
        }
    }
}