package kz.revolut.test.core.data

import kz.revolut.test.converter.data.model.Currency

interface Convertable {
    fun convert(originCurrency: Currency?, amount: Float)
}