package kz.revolut.test.core.data

@Suppress("unused")
sealed class Result<out T: Any> {
    fun value(): T? {
        return if (this is Success) this.data else null
    }

    data class Success<out T : Any>(val data: T) : Result<T>()
    data class Error(val exception: Exception) : Result<Nothing>()
}