package kz.revolut.test.core.data.api

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ApiCurrency(
    val base: String,
    val date: String,
    val rates: Map<String, Float>
) : Parcelable