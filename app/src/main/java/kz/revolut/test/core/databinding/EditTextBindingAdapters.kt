package kz.revolut.test.core.databinding

import android.widget.EditText
import androidx.databinding.BindingAdapter
import kz.revolut.test.core.extension.currencyFormatted

@BindingAdapter("amount")
fun bindingAmount(editText: EditText, amount: Float?) {
    amount?.let {
        editText.setText(amount.currencyFormatted)
    }
}