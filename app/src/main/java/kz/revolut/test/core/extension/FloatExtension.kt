package kz.revolut.test.core.extension

import java.util.*

val Float.currencyFormatted: String
    get() = String.format(Locale.US, "%.2f", this)