package kz.revolut.test.core.lifecycle

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelChildren
import java.util.concurrent.CancellationException
import kotlin.coroutines.CoroutineContext

open class CoroutineContextProvider {
    open val main: CoroutineContext by lazy { Dispatchers.Main }
    open val io: CoroutineContext by lazy { Dispatchers.IO }
}

abstract class SuspendableViewModel(
    coroutineContextProvider: CoroutineContextProvider = CoroutineContextProvider()
): ViewModel(), CoroutineScope {

    val main: CoroutineContext = coroutineContextProvider.main
    val io: CoroutineContext = coroutineContextProvider.io

    @Suppress("MemberVisibilityCanBePrivate")
    protected var job: Job = Job()

    override val coroutineContext: CoroutineContext
        get() = job + main

    override fun onCleared() {
        super.onCleared()
        cancelPendingJobs()
    }

    fun cancelPendingJobs() {
        coroutineContext.cancelChildren(CancellationException())
    }
}