package kz.revolut.test.converter

import junit.framework.Assert.assertEquals
import kz.revolut.test.converter.data.CurrencyConverterImpl
import kz.revolut.test.converter.data.model.Currency
import org.junit.Before
import org.junit.Test

class ConverterTest {

    private lateinit var currencyConverter: CurrencyConverterImpl

    @Before
    fun setup() {
        currencyConverter = CurrencyConverterImpl()
    }

    @Test
    fun `conversion without amount and not base doesn't change anything (no origin currency)`() {
        val list = arrayListOf(
            Currency("AUD", 1.15f, false)
        )

        val convertedList = currencyConverter.convert(list) as List<Currency>
        assertEquals(1.15f, convertedList[0].convertedAmount)
    }

    @Test
    fun `conversion with amount and not base multiplies amount with rate (no origin currency)`() {
        val list = arrayListOf(
            Currency("AUD", 1.15f, false)
        )

        val convertedList = currencyConverter.convert(list, 100f) as List<Currency>
        assertEquals(115f, convertedList[0].convertedAmount)
    }

    @Test
    fun `conversion with amount and not base = amount divided by origin rate (with origin currency)`() {
        val originCurrency = Currency("AUD", 1.15f, false)
        val list = arrayListOf(
            Currency("EUR", 1.0f, true)
        )

        val convertedList = currencyConverter.convert(list, 100f, originCurrency) as List<Currency>
        assertEquals(86.95f, convertedList[0].convertedAmount, 0.01f)
    }

    @Test
    fun `conversion with amount and not base = amount divided by origin rate multiplied by currency rate (with origin currency)`() {
        val originCurrency = Currency("AUD", 1.15f, false)
        val list = arrayListOf(
            Currency("USD", 1.5f, false)
        )

        val convertedList = currencyConverter.convert(list, 100f, originCurrency) as List<Currency>
        assertEquals(130.43f, convertedList[0].convertedAmount, 0.01f)
    }

    @Test
    fun `conversion with amount and base multiplies amount with rate (with origin currency)`() {
        val originCurrency = Currency("EUR", 1.0f, false)
        val list = arrayListOf(
            Currency("EUR", 1.0f, false)
        )

        val convertedList = currencyConverter.convert(list, 100f, originCurrency) as List<Currency>
        assertEquals(100f, convertedList[0].convertedAmount)
    }

    @Test
    fun `conversion with amount and base multiplies amount with rate (with base origin currency)`() {
        val originCurrency = Currency("EUR", 1.0f, true)
        val list = arrayListOf(
            Currency("EUR", 1.0f, true)
        )

        val convertedList = currencyConverter.convert(list, 100f, originCurrency) as List<Currency>
        assertEquals(100f, convertedList[0].convertedAmount)
    }
}