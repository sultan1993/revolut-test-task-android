package kz.revolut.test.model

import kz.revolut.test.converter.data.model.Currency
import org.junit.Assert.assertEquals
import org.junit.Test

class ConvertableTest {

    private var originCurrency = Currency("EUR", 1.0f, true)
    private var currency = Currency("USD", 1.1683f, false)

    @Test
    fun `when converting from origin calculates correct amount`() {
        currency.convert(originCurrency, 100.0f)
        assertEquals(116.83f, currency.convertedAmount, 0.01f)
    }

    @Test
    fun `when converting from origin calculates correct amount (2)`() {
        currency.convert(originCurrency, 10.0f)
        assertEquals(11.683f, currency.convertedAmount, 0.01f)
    }
}