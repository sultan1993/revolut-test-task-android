package kz.revolut.test.model

import com.google.gson.Gson
import kz.revolut.test.converter.data.model.Currency
import kz.revolut.test.converter.data.network.CurrencyAPIMapper
import kz.revolut.test.core.data.api.ApiCurrency
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class CurrencyAPIMapperTest {

    private val testJson = """{"base":"EUR","date":"2018-09-06","rates":{"AUD":1.6232,"BGN":1.964,"BRL":4.8119,
        |"CAD":1.5402,"CHF":1.1322,"CNY":7.9785,"CZK":25.823,"DKK":7.488,"GBP":0.90201,"HKD":9.1707,"HRK":7.4653,
        |"HUF":327.86,"IDR":17396.0,"ILS":4.1881,"INR":84.069,"ISK":128.34,"JPY":130.09,"KRW":1310.2,"MXN":22.459,
        |"MYR":4.8322,"NOK":9.817,"NZD":1.7707,"PHP":62.855,"PLN":4.3364,"RON":4.658,"RUB":79.909,"SEK":10.635,
        |"SGD":1.6067,"THB":38.29,"TRY":7.6602,"USD":1.1683,"ZAR":17.898}}""".trimMargin()

    private lateinit var apiCurrency: ApiCurrency
    private lateinit var currencyList: List<Currency>
    private lateinit var currencyAPIMapper: CurrencyAPIMapper

    @Before
    fun setup() {
        apiCurrency = Gson().fromJson(testJson, ApiCurrency::class.java)
        currencyList = CurrencyAPIMapper.map(apiCurrency)
    }

    @Test
    fun `contains correct size after parsing to domain model`() {
        assertEquals(33, currencyList.size)
    }

    @Test
    fun `base currency has correct base code`() {
        val baseCurrency = currencyList.first { it.isBase }
        assertEquals("EUR", baseCurrency.code)
    }

    @Test
    fun apiMapper_AUDcode_isCorrect() {
        val audCurrency = currencyList.first { it.code == "AUD" }
        assertEquals("AUD", audCurrency.code)
    }

    @Test
    fun apiMapper_AUDrate_isCorrect() {
        val audCurrency = currencyList.first { it.code == "AUD" }
        assertEquals(1.6232f, audCurrency.rate)
    }
}