package kz.revolut.test.model

import com.google.gson.Gson
import kz.revolut.test.core.data.api.ApiCurrency
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test

class CurrencyJSONTest  {

    private val testJson = """{"base":"EUR","date":"2018-09-06","rates":{"AUD":1.6232,"BGN":1.964,"BRL":4.8119,
        |"CAD":1.5402,"CHF":1.1322,"CNY":7.9785,"CZK":25.823,"DKK":7.488,"GBP":0.90201,"HKD":9.1707,"HRK":7.4653,
        |"HUF":327.86,"IDR":17396.0,"ILS":4.1881,"INR":84.069,"ISK":128.34,"JPY":130.09,"KRW":1310.2,"MXN":22.459,
        |"MYR":4.8322,"NOK":9.817,"NZD":1.7707,"PHP":62.855,"PLN":4.3364,"RON":4.658,"RUB":79.909,"SEK":10.635,
        |"SGD":1.6067,"THB":38.29,"TRY":7.6602,"USD":1.1683,"ZAR":17.898}}""".trimMargin()

    private lateinit var apiCurrency: ApiCurrency

    @Before
    fun setup() {
        apiCurrency = Gson().fromJson(testJson, ApiCurrency::class.java)
    }

    @Test
    fun `manual creation is correct`() {
        val currency = ApiCurrency("EUR", "2018-09-24", mapOf("EUR" to 1.0f))
        assertEquals("EUR", currency.base)
    }

    @Test
    fun `base is correct after converting from json`() {
        assertEquals("EUR", apiCurrency.base)
    }

    @Test
    fun `date is correct after converting from json`() {
        assertEquals("2018-09-06", apiCurrency.date)
    }

    @Test
    fun `contains rates after converting from json`() {
        assertNotNull(apiCurrency.rates)
    }

    @Test
    fun `rates size is correct after converting from json`() {
        assertEquals(32, apiCurrency.rates.size)
    }
}