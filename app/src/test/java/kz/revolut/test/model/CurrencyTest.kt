package kz.revolut.test.model

import kz.revolut.test.converter.data.model.Currency
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test

class CurrencyTest {

    private var currency = Currency("USD", 1.1683f, false)

    @Test
    fun `currency name is correct`() {
        assertEquals("USD", currency.code)
    }

    @Test
    fun `currency rate is correct`() {
        assertEquals(1.1683f, currency.rate)
    }

    @Test
    fun `currency base is correct`() {
        assertEquals(false, currency.isBase)
    }

    @Test
    fun `currencies are equal`() {
        val secondCurrency = Currency("USD", 1.1683f, false)
        assertEquals(currency, secondCurrency)
    }

    @Test
    fun `currencies are not equal`() {
        val secondCurrency = Currency("EUR", 1.1683f, true)
        assertNotEquals(currency, secondCurrency)
    }

    @Test
    fun `other object is not equal`() {
        val data = Any()
        assertNotEquals(currency, data)
    }
}