package kz.revolut.test.model

import kz.revolut.test.core.extension.currencyFormatted
import org.junit.Assert.assertEquals
import org.junit.Test

class FloatExtensionTest {

    @Test
    fun `currencyFormatted correctly converts from string`() {
        assertEquals("100.00", 100f.currencyFormatted)
    }

    @Test
    fun `currencyFormatted correctly converts from string (2)`() {
        assertEquals("125.12", 125.12f.currencyFormatted)
    }
}