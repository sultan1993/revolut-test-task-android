package kz.revolut.test.network

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.gson.Gson
import com.nhaarman.mockitokotlin2.mock
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kz.revolut.test.converter.data.model.Currency
import kz.revolut.test.converter.data.network.CurrencyRepository
import kz.revolut.test.core.TestCoroutineRule
import kz.revolut.test.core.data.BaseRepository
import kz.revolut.test.core.data.Result
import kz.revolut.test.core.data.api.ApiCurrency
import okhttp3.ResponseBody
import org.junit.Rule
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

@ExperimentalCoroutinesApi
class BaseRepositoryTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val baseRepository = BaseRepository()

    @Test
    fun `correct job returns success`() = testCoroutineRule.runBlockingTest {
        val result = baseRepository.safeApiCall({
            100 / 10
        })
        assertEquals(true, result is Result.Success)
    }

    @Test
    fun `incorrect job returns failure`() = testCoroutineRule.runBlockingTest {
        val result = baseRepository.safeApiCall({
            100 / 0
        })
        assertEquals(true, result is Result.Error)
    }
}