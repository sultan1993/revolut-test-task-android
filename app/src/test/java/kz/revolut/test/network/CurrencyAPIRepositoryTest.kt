package kz.revolut.test.network

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.gson.Gson
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert.assertEquals
import junit.framework.Assert.fail
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancelChildren
import kz.revolut.test.converter.data.CurrencyConverterImpl
import kz.revolut.test.converter.data.model.Currency
import kz.revolut.test.converter.data.network.CurrencyAPIRepository
import kz.revolut.test.converter.data.network.CurrencyRepository
import kz.revolut.test.converter.data.network.CurrencyService
import kz.revolut.test.core.TestCoroutineRule
import kz.revolut.test.core.data.Result
import kz.revolut.test.core.data.api.ApiCurrency
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.lang.Exception

@ExperimentalCoroutinesApi
class CurrencyAPIRepositoryTest {

    private val testJson = """{"base":"EUR","date":"2018-09-06","rates":{"AUD":1.6232,"BGN":1.964,"BRL":4.8119,
        |"CAD":1.5402,"CHF":1.1322,"CNY":7.9785,"CZK":25.823,"DKK":7.488,"GBP":0.90201,"HKD":9.1707,"HRK":7.4653,
        |"HUF":327.86,"IDR":17396.0,"ILS":4.1881,"INR":84.069,"ISK":128.34,"JPY":130.09,"KRW":1310.2,"MXN":22.459,
        |"MYR":4.8322,"NOK":9.817,"NZD":1.7707,"PHP":62.855,"PLN":4.3364,"RON":4.658,"RUB":79.909,"SEK":10.635,
        |"SGD":1.6067,"THB":38.29,"TRY":7.6602,"USD":1.1683,"ZAR":17.898}}""".trimMargin()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var currencyService: CurrencyService

    private lateinit var currencyRepository: CurrencyRepository

    private lateinit var apiCurrency: ApiCurrency

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        currencyRepository = CurrencyAPIRepository(currencyService)
        apiCurrency = Gson().fromJson(testJson, ApiCurrency::class.java)
    }

    @Test
    fun `repo returns success result on correct response`() = testCoroutineRule.runBlockingTest {
        whenever(currencyService.getLatestCurrencyRates("EUR")).thenReturn(apiCurrency)
        val result = currencyRepository.getLatestRates("EUR")
        assertEquals(true, result is Result.Success)
    }

    @Test
    fun `repo returns error result on null response`() = testCoroutineRule.runBlockingTest {
        whenever(currencyService.getLatestCurrencyRates("EUR")).thenReturn(null)
        val result = currencyRepository.getLatestRates("EUR")
        assertEquals(true, result is Result.Error)
    }

    @Test
    fun `repo returns correct list size`() = testCoroutineRule.runBlockingTest {
        whenever(currencyService.getLatestCurrencyRates("EUR")).thenReturn(apiCurrency)
        val result = currencyRepository.getLatestRates("EUR")

        if (result is Result.Success) {
            val list = result.data
            assertEquals(33, list.size)
        } else {
            fail()
        }
    }

    @Test(expected = CancellationException::class)
    fun `throws JobCancellationException when cancels children`() = testCoroutineRule.runBlockingTest {
        whenever(currencyService.getLatestCurrencyRates("EUR")).thenReturn(apiCurrency)
        currencyRepository.getLatestRates("EUR")
        coroutineContext.cancelChildren()
    }
}