package kz.revolut.test.network

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.mock
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kz.revolut.test.converter.data.CurrencyConverterImpl
import kz.revolut.test.converter.data.model.Currency
import kz.revolut.test.converter.data.network.CurrencyAPIRepository
import kz.revolut.test.converter.data.network.CurrencyRepository
import kz.revolut.test.converter.data.network.CurrencyService
import kz.revolut.test.core.TestCoroutineRule
import kz.revolut.test.core.data.Result
import org.junit.Rule
import org.junit.Test
import java.lang.Exception

@ExperimentalCoroutinesApi
class CurrencyRepositoryManualTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var currencyRepository: CurrencyRepository

    @Test
    fun `fetch result is Success`() = testCoroutineRule.runBlockingTest {
        currencyRepository = object : CurrencyRepository {
            override suspend fun getLatestRates(base: String): Result<List<Currency>> {
                val expectedList = listOf(
                    Currency("EUR", 1.0f, true)
                )
                return Result.Success(expectedList)
            }
        }

        val result = currencyRepository.getLatestRates("EUR")
        assertEquals(true, result is Result.Success)
    }

    @Test
    fun `success result is not null`() = testCoroutineRule.runBlockingTest {
        currencyRepository = object : CurrencyRepository {
            override suspend fun getLatestRates(base: String): Result<List<Currency>> {
                val expectedList = listOf(
                    Currency("EUR", 1.0f, true)
                )
                return Result.Success(expectedList)
            }
        }

        val result = currencyRepository.getLatestRates("EUR")
        if (result is Result.Success) {
            assertEquals(true, result.value() != null)
        }
    }

    @Test
    fun `fetch result is Failure`() = testCoroutineRule.runBlockingTest {
        currencyRepository = object : CurrencyRepository {
            override suspend fun getLatestRates(base: String): Result<List<Currency>> {
                return Result.Error(Exception())
            }
        }

        val result = currencyRepository.getLatestRates("EUR")
        assertEquals(true, result is Result.Error)
    }

    @Test
    fun `fetch result contains items`() = testCoroutineRule.runBlockingTest {
        currencyRepository = object : CurrencyRepository {
            override suspend fun getLatestRates(base: String): Result<List<Currency>> {
                val expectedList = listOf(
                    Currency("EUR", 1.0f, true)
                )
                return Result.Success(expectedList)
            }
        }

        val result = currencyRepository.getLatestRates("EUR")
        if (result is Result.Success) {
            assertEquals(true, result.data.isNotEmpty())
        }
    }
}