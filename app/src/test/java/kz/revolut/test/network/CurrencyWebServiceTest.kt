package kz.revolut.test.network

import kotlinx.coroutines.runBlocking
import kz.revolut.test.converter.data.network.CurrencyService
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class CurrencyWebServiceTest {

    private val testJson = """{"base":"EUR","date":"2018-09-06","rates":{"AUD":1.6232,"BGN":1.964,"BRL":4.8119,
        |"CAD":1.5402,"CHF":1.1322,"CNY":7.9785,"CZK":25.823,"DKK":7.488,"GBP":0.90201,"HKD":9.1707,"HRK":7.4653,
        |"HUF":327.86,"IDR":17396.0,"ILS":4.1881,"INR":84.069,"ISK":128.34,"JPY":130.09,"KRW":1310.2,"MXN":22.459,
        |"MYR":4.8322,"NOK":9.817,"NZD":1.7707,"PHP":62.855,"PLN":4.3364,"RON":4.658,"RUB":79.909,"SEK":10.635,
        |"SGD":1.6067,"THB":38.29,"TRY":7.6602,"USD":1.1683,"ZAR":17.898}}""".trimMargin()

    @get:Rule
    val mockWebServer = MockWebServer()

    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private val currencyService by lazy {
        retrofit.create(CurrencyService::class.java)
    }

    @Test
    fun `on success response has correct base`() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setBody(testJson)
                .setResponseCode(200)
        )

        val apiCurrency = currencyService.getLatestCurrencyRates("EUR")
        assertEquals("EUR", apiCurrency.base)
    }

    @Test(expected = HttpException::class)
    fun `on fail response throws http exception`() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setBody(testJson)
                .setResponseCode(500)
        )

        val apiCurrency = currencyService.getLatestCurrencyRates("EUR")
        assertEquals("EUR", apiCurrency.base)
    }

    @Test
    fun `correct path is requested`() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setBody(testJson)
                .setResponseCode(200)
        )

        var api = currencyService.getLatestCurrencyRates("EUR")
        assertEquals("/latest?base=EUR", mockWebServer.takeRequest().path)
    }
}