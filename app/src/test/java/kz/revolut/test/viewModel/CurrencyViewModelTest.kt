package kz.revolut.test.viewModel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kz.revolut.test.converter.data.CurrencyConverter
import kz.revolut.test.converter.data.model.Currency
import kz.revolut.test.converter.data.network.CurrencyRepository
import kz.revolut.test.converter.ui.ConverterUI
import kz.revolut.test.converter.ui.ConverterViewModel
import kz.revolut.test.core.TestCoroutineRule
import kz.revolut.test.core.data.Result
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class CurrencyViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var converterViewModel: ConverterViewModel

    @Mock
    private lateinit var currencyRepository: CurrencyRepository

    @Mock
    private lateinit var currencyConverter: CurrencyConverter

    @Mock
    private lateinit var converterUIObserver: Observer<ConverterUI>

    @Mock
    private lateinit var loadingObserver: Observer<Boolean>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        converterViewModel = ConverterViewModel(currencyRepository, currencyConverter).apply {
            converterUI.observeForever(converterUIObserver)
            loading.observeForever(loadingObserver)
        }
    }

    @Test
    fun `changed to loading on init`() = testCoroutineRule.runBlockingTest() {
        verify(loadingObserver).onChanged(true)
    }

    @Test
    fun `list loaded after 1 sec`() = testCoroutineRule.runBlockingTest {
        val expectedList = listOf<Currency>()
        whenever(currencyRepository.getLatestRates("EUR")).thenReturn(Result.Success(expectedList))
        converterViewModel.startFetchingRates()
        Thread.sleep(1500)
        verify(converterUIObserver, times(1)).onChanged(ConverterUI.ListLoaded(expectedList))
    }

    @Test
    fun `list loaded 3 times in 3 sec`() = testCoroutineRule.runBlockingTest {
        val expectedList = listOf<Currency>()
        whenever(currencyRepository.getLatestRates("EUR")).thenReturn(Result.Success(expectedList))
        converterViewModel.startFetchingRates()
        Thread.sleep(3500)
        verify(converterUIObserver, atLeast(3)).onChanged(ConverterUI.ListLoaded(expectedList))
    }

    @Test
    fun `changed to loading = false after load`() = testCoroutineRule.runBlockingTest {
        val expectedList = listOf<Currency>()
        whenever(currencyRepository.getLatestRates("EUR")).thenReturn(Result.Success(expectedList))
        converterViewModel.startFetchingRates()
        Thread.sleep(1500)
        verify(loadingObserver, atLeastOnce()).onChanged(false)
    }
}